from functools import reduce
import re
from fuzzywuzzy import fuzz
import json
from datetime import datetime, date
import math
import logging
from common_functions import *
# from EMP.banking import *

from functions.response import *

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def get_epfo_emp_name(epfo_emp_data, qde_name):
    try:
        epfo_emp_name = deep_get(epfo_emp_data['uan'][0], 'employer', 'None')
        for i in epfo_emp_name:
            fuzz_ratio = fuzz.ratio(qde_name, i['matchName'].lower())
            i['fuzz_ratio'] = fuzz_ratio
        max_fuzz_ratio = max([i.get('fuzz_ratio', 0) for i in epfo_emp_name])
        epfo_emp_name = [i.get('matchName') for i in epfo_emp_name if i[
            'fuzz_ratio'] == max_fuzz_ratio]
        if not epfo_emp_name:
            epfo_emp_name = 'epfo emp name not found'
        else:
            epfo_emp_name = epfo_emp_name[0]

        pol_epfo_qde_name_match = True if fuzz.ratio(qde_name,
                                                     epfo_emp_name) >= 81 else False
    except:
        epfo_emp_name = None
        pol_epfo_qde_name_match = False
    return epfo_emp_name, pol_epfo_qde_name_match


def deep_get(dictionary, keys, default=None):
    return reduce(lambda d, key: d.get(key, default) if isinstance(d, dict) else default, keys.split("."), dictionary)


def get_pefios_salary_data(pefios_salary_data, qde_company_name):
    li = []
    for i in pefios_salary_data:
        dict2 = {'date': i['date'], "company_name": i.get('narration', 'None'), 'category': i[
            'category'], 'mob_days': mob_days_perfios(i['date']), 'salary': i['amount'],
                 'fuzz_ratio': fuzz.ratio(qde_company_name, i.get('narration', 'None').lower())}
        li.append(dict2)
    return li


def get_month_wise_salary(perfios_data):
    dict1 = {}
    for i in perfios_data:
        if int(datetime.strptime(i['date'], '%Y-%m-%d').month) in dict1:
            try:
                dict1[int(datetime.strptime(i['date'], '%Y-%m-%d').month)].append(
                    abs(float(i["salary"])))
            except:
                dict1[int(datetime.strptime(i['date'], '%Y-%m-%d').month)].append(0)
        else:
            dict1[int(datetime.strptime(i['date'], '%Y-%m-%d').month)] = [abs(float(i["salary"]))]
    salary_list = [sum(v) for k, v in dict1.items()]
    return salary_list


def mob_days_perfios(dt):
    today = datetime.today()
    sal_date = datetime.strptime(dt, '%Y-%m-%d')
    mob = math.ceil((today - sal_date).days)
    return mob


def uan_mob_calculator(yy_mm):
    try:
        today = datetime.today()
        uan_date = datetime.strptime(f"{yy_mm[:2]}-{yy_mm[2:]}-{today.day}", '%y-%m-%d')
        mob = math.ceil((today - uan_date).days / 30)
    except:
        mob = 100
    return mob


def get_emp_comp_date(epfo_emp_data):
    li = []
    for i in epfo_emp_data:
        dict1 = {}
        dict1['org_name'] = i['name']
        dict1['org_name_confidence'] = i['nameConfidence']
        dict1['org_uan_name_match'] = bool(i['uanNameMatch'])
        dict1['uan_last_month'] = i['lastMonth']
        dict1['emp_uan_mob'] = uan_mob_calculator(i['lastMonth'])
        dict1['emp_score'] = i['emplrScore']
        li.append(dict1)
    return li


class Employment(object):
    def __init__(self, request, conn, conn1, applicationStage):
        self.warnings = {}
        self.conn = conn
        self.conn1 = conn1
        self.data_required_tag = None
        self.request = request
        self.partner = deep_get(self.request, 'report.additionalDetails.partner')
        self.stage = applicationStage
        try:
            self.applicant = self.request['report']['applicants'][0]
        except:
            try:
                self.applicant = self.request['report']['applicant'][0]
            except:
                self.applicant = self.request['report']['applicant']

        self.qde = self.applicant['qde']

        self.email_delivery_status = str(deep_get(self.qde, 'emailDeliveryStatus', 'tag not found'))
        self.pol_delivery_status = False if self.email_delivery_status.lower() in ['bounced',
                                        'dropped', 'in-progress', '', 'tag not found', None] else True
        try:
            self.qde_company_name = str(self.qde['occupationDetails']['companyName']).lower()
        except:
            try:
                self.qde_company_name =str(self.applicant['occupationDetails'][
                                               'companyName']).lower()
            except:
                self.qde_company_name = "companyName not exist"
        try:
           self.qde_name = str(self.qde['firstName']).lower() + ' ' + str(self.qde['middleName']).lower() \
                        + ' ' + str(self.qde['lastName']).lower()
        except:
            self.qde_name = "Name not found"
        self.banking_trigger = None
        self.comp_cat = None
        self.near_prime_segment = None
        self.banking_response = {}
        self.bureau_response = {}
        self.email_response = {}
        self.epfo_response = {}
        self.get_employment_check()

    def get_banking(self):
        war = {}
        employment_verification_data, war = get_banking_from_layer(self.partner, self.conn1,
                                                                   self.request, self.stage)
        pol_delivery_check = True if self.pol_delivery_status is True else False
        if deep_get(employment_verification_data, 'pol_decision_status') == 'Decline' and \
                'pol_delivery_check is False' in employment_verification_data[
            'rejection_reason'] and len(employment_verification_data[
            'rejection_reason']) == 1 and pol_delivery_check is True:
            employment_verification_data['pol_decision_status'] = 'Accept'
            employment_verification_data['pol_delivery_check'] = pol_delivery_check
            employment_verification_data['email_delivery_status'] = self.email_delivery_status

        return employment_verification_data, war

    def get_email(self):
        email_warnings = {}
        pol_delivery_check = True if self.pol_delivery_status is True else False
        try:
            email_data = self.applicant['karzaDetails']['emailDetails']['result']
        except:
            try:
                email_data = self.applicant['karzaDetails']['emailDetails'][
                    'verificationResult']['result']
            except:
                email_data = None

        email_data_status = False if 'error' in deep_get(self.applicant,
                                'karzaDetails.emailDetails.verificationResult', {}).keys() else \
            True

        if email_data is None or not email_data_status:
            email_warnings['email_data'] = ('email_data not present')
            return {
                'pol_org_domain_age': None,
                'pol_org_domain_match': None,
                'clix_org_match_score': None,
                'emp_match_score': None,
                'clix_org_domain_match_score': None,
                'pol_clix_org_domain_match_score': None,
                'pol_emp_match': None,
                'pol_emp_match_score': None,
                'pol_clix_org_match': None,
                'pol_delivery_check': pol_delivery_check,
                'email_delivery_status': self.email_delivery_status,
                'pol_decision_status': 'Decline',
                'rejection_reason': 'email_data not present',
                'reject_review_tag': None,
                'data_required_tag': "EPFO",
                'executed_stage': 'Email',
                'email_data_status': email_data_status,
                'banking_trigger': self.banking_trigger}, email_warnings

        regex = '^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*(\+[a-zA-Z0-9-]+)?@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$'

        emp_email = deep_get(email_data, 'data.email')

        validate_email = True if (re.search(regex, emp_email)) else False

        email_org_domain = None

        if validate_email:
            email_org_domain = emp_email.split('@')[1]

        try:
            pol_emp_match = email_data['additional_info'][
            'individual_match'][0]['match']
        except:
            pol_emp_match = None

        try:
            emp_match_score = email_data['additional_info'][
            'individual_match'][0]['score'] * 100
        except:
            emp_match_score = 0

        pol_emp_match_score = True if emp_match_score >= 81 else False

        try:
            org_domain_list = email_data['additional_info'][
            'company_info']['org_domain_match']
            pol_org_domain_match = any([row['match'] for row in org_domain_list])
        except:
            org_domain_list = []
            pol_org_domain_match = None

        try:
            org_domain_age = email_data['additional_info'][
            'whois_info']['age_year']
        except:
            org_domain_age = 0

        pol_org_domain_age = True if org_domain_age >= 3 else False

        clix_org_domain_match_score = 0

        org_name = None
        if pol_org_domain_match:
            for i in org_domain_list:
                i['domain_match_ratio'] = fuzz.ratio(email_org_domain, i['domain'].lower())
            clix_org_domain_match_score = max([i['domain_match_ratio'] for i in
                                                org_domain_list], default=0)
            try:
                org_name = str([row['org_name'] for row in org_domain_list if
                               row['domain_match_ratio'] == clix_org_domain_match_score][
                                   0]).lower()
            except:
                org_name = 'None'
        clix_org_match_score = fuzz.ratio(org_name, self.qde_company_name)
        pol_clix_org_match = True if clix_org_match_score >= 80 else False

        pol_clix_org_domain_match_score = True if clix_org_domain_match_score >= 85 else False

        # rule 1
        if pol_org_domain_age and pol_org_domain_match and pol_clix_org_domain_match_score and \
                pol_emp_match and pol_emp_match_score and pol_clix_org_match and pol_delivery_check:
            pol_decision_status = "Accept"
        else:
            pol_decision_status = "Decline"

        reject_reason = []
        if pol_decision_status == "Decline":
            if not pol_delivery_check: #and self.condition in [0, 1]:
                reject_reason.append("pol_delivery_check is false")
            if not pol_clix_org_match:
                reject_reason.append("pol_clix_org_match is false")
            if not pol_clix_org_domain_match_score:
                reject_reason.append('pol_clix_org_domain_match_score is false')
            if not pol_org_domain_age:
                reject_reason.append('pol_org_domain_age is false')
            if not pol_org_domain_match:
                reject_reason.append('pol_org_domain_match is false')
            if not pol_emp_match:
                reject_reason.append('pol_emp_match is false')
            if not pol_emp_match_score:
                reject_reason.append('pol_emp_match_score is false')
            # if self.condition == 1:
            #     reject_reason = ["EPFO CHECK FALSE", "move to bureau"]
            self.data_required_tag = "EPFO"

        return {
            'pol_org_domain_age': pol_org_domain_age,
            'pol_org_domain_match': pol_org_domain_match,
            'clix_org_match_score': float("{0:.4f}".format(clix_org_match_score)),
            'clix_org_domain_match_score': float("{0:.4f}".format(clix_org_domain_match_score)),
            'emp_match_score': float("{0:.4f}".format(emp_match_score)),
            'pol_clix_org_domain_match_score': pol_clix_org_domain_match_score,
            'pol_emp_match': pol_emp_match,
            'pol_emp_match_score': pol_emp_match_score,
            'pol_clix_org_match': pol_clix_org_match,
            'pol_delivery_check': pol_delivery_check,
            'email_delivery_status': self.email_delivery_status,
            'pol_decision_status': pol_decision_status,
            'rejection_reason': reject_reason,
            'reject_review_tag': False,
            'data_required_tag': self.data_required_tag,
            'executed_stage': 'Email',
            'banking_trigger': self.banking_trigger}, email_warnings

    def get_epfo(self):
        '''EPFO – name match, company match, MOB '''
        epfo_warnings = {}
        pol_delivery_check = True if self.pol_delivery_status is True else False
        try:
            epfo_emp_data = self.applicant['karzaDetails']['epfoDetails']['result']
        except:
            try:
                epfo_emp_data = self.applicant['karzaDetails']['epfoDetails'][
                    'verificationResult']['result']
            except:
                epfo_emp_data = None

        epfo_data_status = False if 'errorMessage' in deep_get(self.applicant,
                            'karzaDetails.epfoDetails.verificationResult', {}).keys() else True

        if epfo_emp_data is None or not epfo_data_status or not epfo_emp_data['uan']:
            epfo_warnings['epfo_data'] = ('epfo_data not present')
            return {
                'epfo_emp_name': None,
                'pol_epfo_qde_name_match': None,
                'pol_epfo_qde_company_match': None,
                'pol_decision_status': 'Decline',
                'rejection_reason': 'epfo_data not present',
                'reject_review_tag': False,
                'executed_stage': 'EPFO',
                'email_delivery_status': self.email_delivery_status,
                'pol_delivery_check': pol_delivery_check,
                'data_required_tag': 'BUREAU',
                'banking_trigger': self.banking_trigger
            }, epfo_warnings
        try:
            epfo_emp_name = epfo_emp_data['email']['whois']['registrant']['name'].lower()
            pol_epfo_qde_name_match = True if fuzz.ratio(self.qde_name, epfo_emp_name) >= 81 else False
            if pol_epfo_qde_name_match is False:
                epfo_emp_name, pol_epfo_qde_name_match = get_epfo_emp_name(epfo_emp_data, self.qde_name)
        except:
            epfo_emp_name, pol_epfo_qde_name_match = get_epfo_emp_name(epfo_emp_data, self.qde_name)

        epfo_emp_data_list = get_emp_comp_date(epfo_emp_data['uan'][0]['employer'])
        epfo_comp_mob_list = [i['org_name'] for i in epfo_emp_data_list if i['emp_uan_mob'] <= 3]
        pol_epfo_qde_company_match = any(True if fuzz.ratio(self.qde_company_name,
                            i.lower()) >= 85 else False for i in epfo_comp_mob_list)
        reject_reason = []
        if pol_epfo_qde_company_match and pol_epfo_qde_name_match and pol_delivery_check:
            pol_decision_status = "Accept"
            self.data_required_tag = None
        else:
            pol_decision_status = "Decline"
            if pol_epfo_qde_company_match not in [True]:
                reject_reason.append('pol_epfo_qde_company_match is False')
            if pol_epfo_qde_name_match not in [True]:
                reject_reason.append('pol_epfo_qde_name_match is False')
            if not pol_delivery_check:
                reject_reason.append('pol_delivery_check is false')
            self.data_required_tag = "BUREAU"

        return {
            'epfo_emp_name': epfo_emp_name,
            'pol_epfo_qde_name_match': pol_epfo_qde_name_match,
            'pol_epfo_qde_company_match': pol_epfo_qde_company_match,
            'pol_decision_status': pol_decision_status,
            'rejection_reason': reject_reason,
            'reject_review_tag': False,
            'executed_stage': 'EPFO',
            'email_delivery_status': self.email_delivery_status,
            'pol_delivery_check': pol_delivery_check,
            'data_required_tag': self.data_required_tag,
            'banking_trigger': self.banking_trigger
        }, epfo_warnings

    def get_bureau(self, banking_executed):
        bureau_warnings = {}
        try:
            # create a cursor
            cursor = self.conn1.cursor()
        except Exception as e:
            logger.error(e)
            bureau_warnings["DataBase"] = "database connection break!"
            return {}, bureau_warnings
        query = "select response_json from req_resp_scorecards where application_stage = " \
                "'bureau' and " \
                "loan_app_id = '{}' order by id desc;".format(self.request["applicationId"])
        cursor.execute(query)
        data = cursor.fetchone()
        if data is not None and len(data) != 0:
            try:
                root_value = data[0]['bureau_response']['calculated_variables']
            except:
                root_value = data[0]['primeResponse']['bureau_response']['calculated_variables']
            ev_comp_bureau_match = deep_get(root_value, 'ev_bureau_match')
            self.banking_trigger = deep_get(root_value, 'banking_trigger')
            policy_type = deep_get(root_value, 'policy_type')
            comp_cat = deep_get(root_value, 'comp_cat')
            segment = deep_get(root_value, 'segment')
        else:
            bureau_warnings["ApplicationNotFound"] = 'No application found in db for consumer ' \
                                                   'stage'
            ev_comp_bureau_match = False
            comp_cat = None
            segment = None
            policy_type = 'Not found'

        if (policy_type in ['PRIME'] and comp_cat in ['GOVT', 'govt']) or (
                policy_type in ['NEAR-PRIME'] and segment in ['A1']):
            reject_reason = []
            pol_delivery_check = True if self.pol_delivery_status is True else False
            pol_ev_comp_bureau_match = ev_comp_bureau_match
            reject_review_tag = None
            if pol_ev_comp_bureau_match and pol_delivery_check:
                pol_decision_status = "Accept"
                self.data_required_tag = None
            else:
                pol_decision_status = 'Refer'
                reject_review_tag = True
                if not pol_ev_comp_bureau_match:
                    reject_reason.append('pol_ev_bureau_match is false')
                if not pol_delivery_check:
                    reject_reason.append('pol_delivery_check is false')
                if 'banking' not in self.stage and self.banking_trigger in [False, None] and \
                        not banking_executed:
                    self.data_required_tag = 'banking'
                    pol_decision_status = 'Decline'
                    reject_review_tag = False
                elif 'banking' in self.stage and self.banking_trigger in [False, None] and not banking_executed:
                    return self.get_banking()
                else:
                    self.data_required_tag = None
        else:
            self.warnings['Consumer'] = 'Bureau Decision Not Applicable'
            self.data_required_tag = "banking"
            pol_ev_comp_bureau_match = None
            pol_delivery_check = None
            reject_reason = [f'Bureau Decision Not Applicable as comp_cat is {comp_cat} ,'
                             f'policy_type is {policy_type} and segment is {segment}']
            reject_review_tag = None
            self.banking_trigger = None
            pol_decision_status = 'Decline'
        return {'pol_decision_status': pol_decision_status,
                'rejection_reason': reject_reason, 'reject_review_tag': reject_review_tag,
                'data_required_tag': self.data_required_tag,
                'pol_ev_bureau_match': pol_ev_comp_bureau_match,
                'pol_delivery_check': pol_delivery_check,
                'executed_stage': 'Consumer',
                'banking_trigger': self.banking_trigger,
                'email_delivery_status': self.email_delivery_status,
                'policy_type': policy_type,
                'comp_cat': comp_cat,
                'segment': segment
                }, bureau_warnings

    def get_employment_check(self):
        final_resp = {}
        war = {}
        banking_executed = False
        if 'banking' in self.stage:
            self.banking_response, self.warnings['banking'] = self.get_banking()
            final_resp = self.banking_response
            banking_executed = True

        if 'email' in self.stage or ('email' in self.stage and 'banking' in self.stage
        and self.banking_response.get('pol_decision_status', '') == 'Decline'):
            self.email_response, self.warnings['email'] = self.get_email()
            final_resp = self.email_response

        if 'epfo' in self.stage or ('epfo' in self.stage and 'email' in self.stage and \
                self.email_response.get('pol_decision_status', '') == 'Decline'):
            self.epfo_response, self.warnings['epfo'] = self.get_epfo()
            final_resp = self.epfo_response

            if self.epfo_response.get('pol_decision_status', '') == 'Decline' and 'consumer' in\
                    self.stage:
                self.bureau_response, self.warnings['bureau'] = self.get_bureau(banking_executed)
                final_resp = self.bureau_response

        if 'consumer' in self.stage and 'epfo' in self.stage and \
                self.epfo_response.get('pol_decision_status', '') == 'Decline':
            self.bureau_response, self.warnings['bureau'] = self.get_bureau(banking_executed)
            final_resp = self.bureau_response

        if not self.email_response and not self.epfo_response and not self.banking_response and \
                not self.bureau_response:
            return {'rejection_reason': 'applicant_stage did not match',
                    'pol_decision_status': 'Decline'}, final_resp, self.warnings
        return {**{**{'email_resp': self.email_response}, **{'banking_resp': self.banking_response}},
                **{**{'consumer_resp': self.bureau_response}, **{'epfo_resp':
                                                self.epfo_response}}}, final_resp, self.warnings




