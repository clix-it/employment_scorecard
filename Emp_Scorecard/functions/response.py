import os
import json
import boto3
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def warning_resp_json(event, env, warnings):
    return {"status": False,
            "loanApplicationId": event["applicationId"],
            "customerId": event["customerId"],
            "product": event["product"],
            "partner": event["partner"],
            "stage": event["report"]["stage"]["applicationStage"].lower(),
            "scorecard": {
                "scorecard_name": f"{env}:D2C_Scorecard",
                "scorecard_version": "1.0"
            },
            "warnings": warnings}


def response_json(request, response, tradeline_list=None, warnings=None, application_stage=None):
    product = request["product"]
    partner = request["partner"]
    app_id = request["applicationId"]
    customer_id = request["customerId"]

    try:
        pan = request["report"]["applicant"]["qde"]["pan"]
    except:
        pan = None

    if tradeline_list is None:
        tradeline_list = []

    if warnings is None:
        warnings = {}

    try:
        reasons = response["final_resp"]['rejection_reason']
    except:
        reasons = response.get('rejection_reason')

    if len(warnings) == 0:
        warnings['No warnings'] = 'Correct data'
    resp_dict = {
        "status": "true",
        "loanApplicationId": app_id,
        "customerId": customer_id,
        "product": product,
        "partner": partner,
        "scorecard": {
            "scorecard_name": f"{os.environ['env']}:EV_Scorecard",
            "scorecard_version": "1.0"
        },
        "bureau_response": {
            "calculated_variables":
                response,
            "decision": {
                "status": response.get("final_resp", {}).get('pol_decision_status', "Decline"),
                "reasons": reasons,
                "reject_review": response.get("final_resp", {}).get('reject_review_tag'),
                "executed_stage": str(response.get("final_resp", {}).get('executed_stage')).upper(),
                "data_required_tag": str(response.get("final_resp", {}).get('data_required_tag')).upper()
            },
            "tradelines": tradeline_list,
            "eligibility": {},
            "extra_details": {
                "document_list": [],
                "applicationStage": application_stage
            }
        },
        "warnings": warnings

    }

    query_dict = {"product": product, "partner": partner, "scorecard_version": "$LATEST",
                  "loan_app_id": app_id,
                  "customer_id": customer_id, "calculated_variables": json.dumps(response),
                  "mobile_no": response.get("mobile_number"), "pan_no": pan,
                  "status": response.get("final_resp", {}).get('pol_decision_status'),
                  "reject_reasons": reasons,
                  "eligibility": json.dumps([]),
                  "loanAmount": None,
                  "extra_details": json.dumps([]),
                  "warnings": json.dumps(warnings)
                  }

    lambda_client = boto3.client('lambda')
    log_response = lambda_client.invoke(FunctionName="scorecard_async_logging",
                                        InvocationType='Event',
                                        Payload=json.dumps(query_dict))
    log_resp = log_response['Payload'].read().decode()
    logger.info("---------Scoreacrd logs Response---------:")
    logger.info(log_resp)

    return resp_dict
