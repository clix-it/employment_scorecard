import numpy as np
import math
from datetime import datetime, date
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def overdue_fun(row):
    try:
        if not math.isnan(row['overdue_amt']) and row['overdue_amt'] not in [None] and int(row['overdue_amt']) > \
                5000:##row['mob'] < 60: # and row["acct_type_id"] not in ["A15", "A16", "A17"]:
            count = 1
        else:
            count = 0
    except:
        count = 0

    return count


def perfios_customer_info(applicant):
    try:
        summary_info = applicant['bankDetails'][0]['summaryInfo']['map']
    except:
        try:
            summary_info = applicant['bankDetails'][0]['analysisData']['summaryInfo']
        except:
            summary_info = {}
    try:
        customer_info = applicant['bankDetails'][0]['customerInfo']['map']
    except:
        try:
            customer_info = applicant['bankDetails'][0]['analysisData']['customerInfo']
        except:
            customer_info = {}
    try:
        pf_instName = summary_info["instName"]  # Bank Name
    except:
        pf_instName = None
    try:
        pf_accType = summary_info["accType"]
    except:
        pf_accType = None
    try:
        pf_accNo = summary_info["accNo"]
    except:
        pf_accNo = None
    try:
        pf_bank_name = customer_info["bank"]  # Bank Name
    except:
        pf_bank_name = None
    try:
        pf_address = customer_info["address"]
    except:
        pf_address = None
    try:
        pf_name = customer_info["name"]
    except:
        pf_name = None
    try:
        pf_mobile = customer_info["mobile"]
    except:
        pf_mobile = None
    try:
        pf_pan = customer_info["pan"]
    except:
        pf_pan = None
    return {"pf_accType": pf_accType, "pf_accNo": pf_accNo, "pf_instName": pf_instName, "pf_bank_name": pf_bank_name,
            "pf_address": pf_address, "pf_name": pf_name, "pf_mobile": pf_mobile, "pf_pan": pf_pan}

# def acct_type_cleaner(acct_type_dict, tradelines):
#     for row in tradelines:
#         row['acct_type_id'] = acct_type_dict[row['acct_type_id']][0]
#     return tradelines


def acct_type_cleaner(acct_type_dict, tradelines):
    msg = None
    for row in tradelines:
        try:
            if int(row['acct_type_id']) == 0:
                row['acct_type_id'] = "A32"
            else:
                row['acct_type_id'] = acct_type_dict[int(row['acct_type_id'])][0]
        except Exception as E:
            logger.info(E)
            row['acct_type_id'] = "A32"
    return tradelines


def live_indicator_fun(row):
    if row['current_bal'] <= 0.0 or row['current_bal'] in ["", None]:
        live_indicator = 0
    else:
        live_indicator = 1
    return live_indicator


def days_calculator(birth_date):
    if birth_date is None or birth_date in [""]:
        return None
    birth_date = birth_date.replace('/', '-').replace(' ', '').replace(',', '')
    if birth_date is None or birth_date in [""]:
        return None
    try:
        birth_date = datetime.strptime(birth_date, '%d-%m-%Y')
        today = datetime.today()
        days = (today - birth_date).days
        return int(days)
    except Exception as exception:
        return None


# def last_payment_days_fun(row):
#     try:
#         x = (row['reported_date'] - row['last_payment_date']).days
#     except:
#         x = 0
#     return x

def industry_str_cleaner(string):
    if string is not None:
        rm_slash = string.replace(" / ", " ").replace("/ ", " ").replace(" /", " ").replace("/", " ")
        pipe_str = rm_slash.replace(" ", "|").replace("(", "").replace(
            ")", "").replace(",", "").replace("-", "").upper()
        return pipe_str
    else:
        return None


def calculated_variables(tadelines):
    count_morat_flag = 0
    live_account_list = []
    pol_only_gold_loan_list = []
    pol_bl_loan_list = []
    pol_only_guarantor_list = []
    no_derog_36mths_list = []
    pol_no_derog_ever_list = []
    nbr_pl_3mths_list = []
    nbr_pl_6mths_list = []
    pol_only_CD_list = []
    pol_90_plus_ever_list = []
    pol_60_plus_12mth_list = []
    pol_30_plus_6mth_list = []
    pol_0_plus_current = []
    nbr_pl_12mths_list = []
    mob_list = []
    max_mob_all_list = []
    overdue_list = []
    pol_90_plus_36mth_list = []

    for row in tadelines:
        count_morat_flag = count_morat_flag + int(row['morat_flag'])
        live_account_list.append(0 if row['live_account'] is None else row['live_account'])
        pol_only_gold_loan_list.append(row['pol_only_gold_loan'])
        pol_bl_loan_list.append(row['pol_bl_loan'])
        pol_only_guarantor_list.append(row['pol_only_guarantor'])
        no_derog_36mths_list.append(row['no_derog_36mths'])
        pol_no_derog_ever_list.append(row['pol_no_derog_ever'])
        nbr_pl_3mths_list.append(row['nbr_pl_3mths'])
        nbr_pl_6mths_list.append(row['nbr_pl_6mths'])
        pol_only_CD_list.append(row['pol_only_CD'])
        pol_90_plus_ever_list.append(row['pol_90_plus_ever'])
        pol_60_plus_12mth_list.append(row['pol_60_plus_12mth'])
        pol_30_plus_6mth_list.append(row['pol_30_plus_6mth'])
        pol_0_plus_current.append(row['pol_0_plus_current'])
        nbr_pl_12mths_list.append(row['nbr_pl_12mths'])
        mob_list.append(row["mob"])
        max_mob_all_list.append(row['max_mob_all'])
        overdue_list.append(row['overdue'])
        pol_90_plus_36mth_list.append(row['pol_90_plus_36mth'])

    return count_morat_flag, live_account_list, pol_only_gold_loan_list, pol_bl_loan_list, pol_only_guarantor_list, \
           no_derog_36mths_list, pol_no_derog_ever_list, nbr_pl_3mths_list, nbr_pl_6mths_list, \
           pol_only_CD_list, pol_90_plus_ever_list, pol_60_plus_12mth_list, pol_30_plus_6mth_list, pol_0_plus_current, \
           nbr_pl_12mths_list, mob_list, max_mob_all_list,overdue_list, pol_90_plus_36mth_list


def del_pl_tradeline(tradelines):
    try:
        for row in tradelines:
            if row['acct_type_id'] == 'A12' and row['current_bal'] == 0 and (row['mob'] < 3 or row['open_months'] < 3) \
                    and row['sanctioned_amount'] > 100000:
                tradelines.remove(row)
        return tradelines
    except:
        return tradelines

# 10.1
# def max_mob_all_fun(row):
#     try:
#         if row['ownership_tag'] is True:
#             disbursed_date = datetime.strptime(row['disbursed_dt'], '%Y-%m-%d').date()
#             x = datetime.strptime(row['source_date'], '%d-%m-%Y').date()
#             mob = math.ceil((x - disbursed_date).days / 30)
#             return mob
#         else:
#             return 0
#     except Exception as error:
#         return 0

# 10.3
# def open_months_fun(row):
#     try:
#         disbursed_date = datetime.strptime(row['disbursed_dt'], '%Y-%m-%d').date()
#         x = datetime.strptime(row['close_dt'], '%Y-%m-%d').date()
#         open_mnth = math.ceil((x - disbursed_date).days / 30)
#         return open_mnth
#     except Exception as error:
#         return 0




