import json, os
import operator
import psycopg2
import logging
import boto3
from getBureau import *
from config.connection import *
from functions.response import *
from common_functions import *
from functions.conf_functions import *
from EMP.employment_scorecard import *
import sentry_sdk
from sentry_sdk.integrations.aws_lambda import AwsLambdaIntegration

logger = logging.getLogger()
logger.setLevel(logging.INFO)

if os.environ["is_sentry"].lower() in ['true']:
    sentry_sdk.init(dsn=os.environ["sentry_dsn"], integrations=[AwsLambdaIntegration()], environment=os.environ["env"])

client = boto3.client('lambda')
presponse = client.invoke(
        FunctionName=os.environ['GET_PASSWORD_ARN'],
        InvocationType='RequestResponse',
        Payload=json.dumps({"queryStringParameters": {"secret_name": "delphi2_psql"}})
    )
precords = json.load(presponse['Payload'])
password = dict(precords).get('body').get('password')
try:
    # connect to the PostgreSQL server
    logger.info("Creating connections...")
    conn = psycopg2.connect(host=os.environ['host'], user=os.environ['user'], password=password,
                            database=os.environ['database'], port=os.environ['port'])
    logger.info("Connection successful")
    error = None
except Exception as e:
    logger.error(e)
    error = 'Connection Failed'
    logger.error(error)

try:
    # connect to the PostgreSQL server
    logger.info("Creating connections...")
    conn1 = psycopg2.connect(host=os.environ['host'], user=os.environ['user'], password=password,
                            database=os.environ['database1'], port=os.environ['port'])
    logger.info("Connection successful")
    error = None
except Exception as e:
    logger.error(e)
    error = 'Connection Failed'
    logger.error(error)


def get_scorecard(event, context):
    logger.info(f"Loading {os.environ['env']} EVScorecard")
    # #if os.environ['env'] not in ["UAT", "Production", "PROD"]:
    # event = open('functions/bureau.json')
    # event = json.load(event)
    warnings = {}

    if error is not None:
        warnings["DataBase Info"] = error
        return warning_resp_json(event, os.environ['env'], warnings)

    try:
        applicationStage = event["report"]["stage"]["applicationStage"].lower()
    except:
        applicationStage = None

    if applicationStage is None:
        warnings["applicationStage"] = 'applicationStage is not present in json'
        return warning_resp_json(event, os.environ['env'], warnings)
    applicationStage = applicationStage.lower().split('|')

    emp = Employment(event, conn, conn1, applicationStage)
    pq_employment_data, final_resp, war = emp.get_employment_check()
    pq_employment_data['final_resp'] = final_resp

    logger.info(pq_employment_data)

    return response_json(event, pq_employment_data, warnings={**warnings, **war},
                         application_stage=applicationStage)
